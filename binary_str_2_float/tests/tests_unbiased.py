from converter import binary_number_string_parser , main
from nose.tools import with_setup, raises

def test_trivial():
    "tests that the trivial case 0.1e1 == 1."
    assert binary_number_string_parser("0.1e1") == 1.


def test_less_trivial():
    "tests that the trivial case 0.1111010101010e-4 == 0.0598907470703 up to 1e-10 accuracy"
    assert (binary_number_string_parser("0.1111010101010e-100") - 0.0598907470703)/0.0598907470703 < 1e-10

def test_negative_sign():
    "tests for the correct interpretation of negative sign"
    assert binary_number_string_parser("-0.1e1") == -1.
    
def test_positive_sign():
    "tests for the correct interpretation of positive sign"
    assert binary_number_string_parser("+0.1e1") == +1.

def test_negative_exponent():
    "tests for the correct interpretation of negative exponent"
    assert binary_number_string_parser("+0.1e-1") == 0.25


def test_explicit_positive_exponent():
    "tests for the correct interpretation of positive exponent"
    assert binary_number_string_parser("+0.1e+10") == 2.

import sys
argv_copy = []
def setup_function():
    argv_copy[:] = [x for x in sys.argv]

def teardown_function():
    sys.argv[:] = [x for x in argv_copy]


@with_setup(setup_function,teardown_function)
def test_command_line():
    """checks whether line command responds properly"""
    sys.argv[1] = "0.1111010101010e-100"
    main()


@raises(Exception)
def test_must_throw_if_non_normalized_1():
    """Test that we deal only with normalized representations"""
    binary_number_string_parser("+0.01e+10")

@raises(Exception)
def test_must_throw_if_non_normalized_2():
    """Test that we deal only with normalized representations"""
    binary_number_string_parser("+1e+10")








